import re
from flask import Flask ,render_template,request ,redirect,url_for,flash,session
from flask_wtf import FlaskForm
from wtforms import PasswordField,SubmitField
from wtforms import validators 
from flask_sqlalchemy import SQLAlchemy 
from datetime import datetime
from os import urandom

    
app = Flask(__name__)


app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///try.db"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY']='%s%s'%(urandom(50),urandom(50))
db = SQLAlchemy(app)
  
class Info_form(FlaskForm):
    name = PasswordField('')
    submit = SubmitField('Submit')

class Todo(db.Model):
    # __searchable__ =['name','MOBILE_NUMBER','EMAIL']
    srno = db.Column(db.Integer,primary_key =True)
    name = db.Column(db.String(60),nullable=False, unique = True)
    IMG = db.Column(db.String,nullable=False )
    GENDER = db.Column(db.String,nullable=False)
    BIRTH_DATE = db.Column(db.String,nullable=False)
    MOBILE_NUMBER = db.Column(db.String,nullable=False)
    EMAIL = db.Column(db.String,nullable=False)
    ###########################################
    Address = db.Column(db.String,nullable=False)
    CITY = db.Column(db.String,nullable=False)
    PIN_CODE = db.Column(db.String,nullable=False)
    ############################################
    college = db.Column(db.String,nullable=False)
    std = db.Column(db.String,nullable=False)
    Timeng =db.Column(db.String,nullable=False)
    OPTIONAL_SUBJECTS = db.Column(db.String,nullable=False)
    ######################################################
    PREVIOUS_STANDARD=db.Column(db.String,nullable=False)
    PREVIOUS_PERCENTAGE = db.Column(db.String,nullable=False)
    PREVIOUS_SCHOOL = db.Column(db.String,nullable=False)
    PREVIOUS_CLASSES = db.Column(db.String,nullable=False)
    #######################################################
    PREVIOUS2_STANDARD=db.Column(db.String)
    PREVIOUS2_PERCENTAGE = db.Column(db.String)
    PREVIOUS2_SCHOOL = db.Column(db.String)
    PREVIOUS2_CLASSES = db.Column(db.String)
    #######################################################
    FATHER_NAME=db.Column(db.String,nullable=False)
    FATHER_QUALIFICATIO = db.Column(db.String)
    FATHER_OCCUPATIO = db.Column(db.String,nullable=False)
    FATHER_MOBILE = db.Column(db.String,nullable=False)
    FATHER_EMAIL = db.Column(db.String)
    ######################################3##########
    MOM_NAME=db.Column(db.String,nullable=False)
    MOM_QUALIFICATIO = db.Column(db.String)
    MOM_OCCUPATIO = db.Column(db.String)
    MOM_MOBILE = db.Column(db.String)
    MOM_EMAIL = db.Column(db.String)
    dat_ = db.Column(db.DateTime,default=datetime.utcnow)

    def __repr__(self) -> str:
        return f'{self.srno} - {self.name}'


@app.route('/')
def Home():
    form = Info_form()
    return render_template('home.html',form=form)
    
@app.route('/Admin',methods=['GET','POST'])
def Admin():
    form = Info_form()
    if form.validate_on_submit():
        session['name']= form.name.data
        session['submit']= form.submit.data
        if  session['name'] == '123':
            return redirect (url_for('Admin_User_Data'))
        else:
            flash('          Invalid Password')
            return redirect(url_for('Home'))
        # return render_template('home.html',form=form)
    return render_template('home.html',form=form)


@app.route('/Student',methods = ['POST','GET'])
def st():
    if request.method == "POST":
        a = {
             'FULL': request.form.get('FNAME').capitalize()+' '+request.form.get('MNAME').capitalize()+" "+request.form.get('LNAME').capitalize(),
            'GENDER':request.form.get('GENDER'),
            'IMG':request.form.get('IMG'),
            'BIRTH_DATE':request.form.get('BIRTH'),
            'MOBILE_NUMBER':request.form.get('SMOBILE'),
            'EMAIL':request.form.get('SEMAIL'),
            'Address':request.form.get('Address'),
            'CITY':request.form.get('city'),
            'PIN_CODE':request.form.get('PIN'),
            'college':request.form.get('Clg'),
            'std':request.form.get('STD'),
            'Timeng':request.form.get('TIMG'),
            'OPTIONAL_SUBJECTS':request.form.get('SUBJECTS')+'  , '+request.form.get('LANGUAGE'),
            'PREVIOUS_STANDARD':request.form.get('PSTD'),
            'PREVIOUS_PERCENTAGE':request.form.get('PPER'),
            'PREVIOUS_SCHOOL':request.form.get('PSC'),
            'PREVIOUS_CLASSES':request.form.get('PCLS'),
            'PREVIOUS2_STANDARD':request.form.get('PPSTD'),
            'PREVIOUS2_PERCENTAGE':request.form.get('PPPER'),
            'PREVIOUS2_SCHOOL':request.form.get('PPSC'),
            'PREVIOUS2_CLASSES':request.form.get('PPCLASS'),
            'FATHER_NAME':request.form.get('FN'),
            'FATHER_QUALIFICATIO':request.form.get('FQ'),
            'FATHER_OCCUPATIO':request.form.get('FO'),
            'FATHER_MOBILE':request.form.get('FM'),
            'FATHER_EMAIL':request.form.get('FE'),
            'MOM_NAME':request.form.get('MN'),
            'MOM_QUALIFICATIO':request.form.get('MQ'),
            'MOM_OCCUPATIO':request.form.get('MO'),
            'MOM_MOBILE':request.form.get('MM'),
            'MOM_EMAIL':request.form.get('ME')}
        try:
            SEND_DATA = Todo(name=a['FULL'],IMG=a['IMG'],GENDER=a['GENDER'],BIRTH_DATE=a['BIRTH_DATE'],
        MOBILE_NUMBER =a['MOBILE_NUMBER'],EMAIL=a['EMAIL'],Address=a['Address'],
        CITY=a['CITY'],PIN_CODE=a['PIN_CODE'],college=a['college'],std=a['std'],
        Timeng = a['Timeng'],OPTIONAL_SUBJECTS=a['OPTIONAL_SUBJECTS'],
        PREVIOUS_STANDARD=a['PREVIOUS_STANDARD'],PREVIOUS_PERCENTAGE=a['PREVIOUS_PERCENTAGE'],PREVIOUS_SCHOOL=a['PREVIOUS_SCHOOL'],PREVIOUS_CLASSES=a['PREVIOUS_CLASSES'],PREVIOUS2_STANDARD=a['PREVIOUS2_STANDARD'],PREVIOUS2_PERCENTAGE = a['PREVIOUS2_PERCENTAGE'],PREVIOUS2_SCHOOL=a['PREVIOUS2_SCHOOL'],PREVIOUS2_CLASSES=a['PREVIOUS2_CLASSES'],FATHER_NAME=a['FATHER_NAME'],FATHER_QUALIFICATIO=a['FATHER_QUALIFICATIO'],FATHER_OCCUPATIO=a['FATHER_OCCUPATIO'],FATHER_MOBILE=a['FATHER_MOBILE'],FATHER_EMAIL=a['FATHER_EMAIL'],MOM_NAME=a['MOM_NAME'],MOM_QUALIFICATIO=a['MOM_QUALIFICATIO'],
        MOM_OCCUPATIO=a['MOM_OCCUPATIO'],MOM_MOBILE=a['MOM_MOBILE'],MOM_EMAIL=a['MOM_EMAIL'])
            db.session.add(SEND_DATA)
            db.session.commit()
            flash('HAY YOU WELCOME TO PREMA GROUP ')
            return redirect(url_for('Home'))
        except:
            return render_template('for.html')
    return render_template('for.html')


@app.route('/27427347628467234672347623764237423472374237842874286467328467238467234723472@$@@$@$@$@^^@#%^@#@#%@^#%@^#%@#%%#%#%#%#%#%#%#%#%#%#%#%%%@#%@#%@#%@%#@%#%@#%@!@!@!@@!@@!@!@!@!@@!@!@!@!@!@@!@!@!@!@!@!@!@!@!@!@#&^3dhdahdakhjkdhjakhdjdjhadaqqweqwuyeuqeyqeyuqeyuqeuiqeyqequi@$^*267ghdahgdgsduyiqeyqwueyqueeuywqeuqwsjhasdadhadhjasdjuuy7')
def Admin_User_Data():
    all_data = Todo.query.all()
    return render_template('gg.html',all_data=all_data)

@app.route('/del/<int:srno>')
def delete(srno):
    del_ = Todo.query.filter_by(srno=srno).first()
    db.session.delete(del_)
    db.session.commit()
    return redirect(url_for('Admin_User_Data'))

@app.route('/Up/<int:srno>')
def UPDATE(srno):
    up = Todo.query.filter_by(srno=srno).first()
    return render_template('Update.html',up=up)

@app.route('/SERCH', methods = ['GET','POST'])
def  Serch(): 
    if request.method == 'POST':
        Nam = request.form.get('GGG')
        up = Todo.query.filter_by(name=Nam).first()
        return render_template('navbar.html',up=up)


@app.route('/VIEW/<int:srno>' , methods = ['GET',"POST"] )
def VIEW(srno):
    if request.method == 'POST':
        b = {
             'FULL': request.form.get('FNAME').capitalize(),
            'GENDER':request.form.get('GENDER'),
            'IMG':request.form.get('IMG'),
            'BIRTH_DATE':request.form.get('BIRTH'),
            'MOBILE_NUMBER':request.form.get('SMOBILE'),
            'EMAIL':request.form.get('SEMAIL'),
            'Address':request.form.get('Address'),
            'CITY':request.form.get('city'),
            'PIN_CODE':request.form.get('PIN'),
            'college':request.form.get('Clg'),
            'std':request.form.get('STD'),
            'Timeng':request.form.get('TIMG'),
            'OPTIONAL_SUBJECTS':request.form.get('SUBJECTS'),
            'PREVIOUS_STANDARD':request.form.get('PSTD'),
            'PREVIOUS_PERCENTAGE':request.form.get('PPER'),
            'PREVIOUS_SCHOOL':request.form.get('PSC'),
            'PREVIOUS_CLASSES':request.form.get('PCLS'),
            'PREVIOUS2_STANDARD':request.form.get('PPSTD'),
            'PREVIOUS2_PERCENTAGE':request.form.get('PPPER'),
            'PREVIOUS2_SCHOOL':request.form.get('PPSC'),
            'PREVIOUS2_CLASSES':request.form.get('PPCLASS'),
            'FATHER_NAME':request.form.get('FN'),
            'FATHER_QUALIFICATIO':request.form.get('FQ'),
            'FATHER_OCCUPATIO':request.form.get('FO'),
            'FATHER_MOBILE':request.form.get('FM'),
            'FATHER_EMAIL':request.form.get('FE'),
            'MOM_NAME':request.form.get('MN'),
            'MOM_QUALIFICATIO':request.form.get('MQ'),
            'MOM_OCCUPATIO':request.form.get('MO'),
            'MOM_MOBILE':request.form.get('MM'),
            'MOM_EMAIL':request.form.get('ME')}
        
        todo = Todo.query.filter_by(srno=srno).first()
        todo.name =b['FULL']
        todo.IMG = b['IMG']
        todo.BIRTH_DATE = b['BIRTH_DATE']
        todo.MOBILE_NUMBER = b['MOBILE_NUMBER']
        todo.EMAIL = b['EMAIL']
        todo.Address = b['Address']
        todo.CITY = b['CITY']
        todo.PIN_CODE = b['PIN_CODE']
        todo.college = b['college']
        todo.std = b['std']
        todo.Timeng = b['Timeng']
        todo.OPTIONAL_SUBJECTS = b['OPTIONAL_SUBJECTS']
        todo.PREVIOUS_STANDARD = b['PREVIOUS_STANDARD']
        todo.PREVIOUS_PERCENTAGE = b['PREVIOUS_PERCENTAGE']
        todo.PREVIOUS_SCHOOL = b['PREVIOUS_SCHOOL']
        todo.PREVIOUS_CLASSES = b['PREVIOUS_CLASSES']
        todo.PREVIOUS2_STANDARD = b['PREVIOUS2_STANDARD']
        todo.PREVIOUS2_PERCENTAGE = b['PREVIOUS2_PERCENTAGE']
        todo.PREVIOUS2_SCHOOL = b['PREVIOUS2_SCHOOL']
        todo.PREVIOUS2_CLASSES = b['PREVIOUS2_CLASSES']
        todo.FATHER_NAME = b['FATHER_NAME']
        todo.FATHER_QUALIFICATIO = b['FATHER_QUALIFICATIO']
        todo.FATHER_OCCUPATIO = b['FATHER_OCCUPATIO']
        todo.FATHER_MOBILE = b['FATHER_MOBILE']
        todo.FATHER_EMAIL = b['FATHER_EMAIL']
        todo.MOM_NAME = b['MOM_NAME']
        todo.MOM_QUALIFICATIO = b['MOM_QUALIFICATIO']
        todo.MOM_OCCUPATIO = b['MOM_OCCUPATIO']
        todo.MOM_MOBILE = b['MOM_MOBILE']
        todo.MOM_EMAIL = b['MOM_EMAIL']
        db.session.add(todo)
        db.session.commit()
        return redirect(url_for('Admin_User_Data'))
    todo =   Todo.query.filter_by(srno=srno).first()   
    return render_template('view.html',up=todo)



if __name__ == '__main__':
    app.run(debug=0)
